


#ifndef __SENSOR_H
#define __SENSOR_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct{
		int val;
		char *name;
}dir_t;

typedef struct{
		dir_t led;
		dir_t tmp;
		dir_t hum;
}sensor_t;

int sensor_int();
int sensor_data_set(char *str);
int sensor_data_get();
#ifdef __cplusplus
}
#endif

#endif /* __SENSOR_H */







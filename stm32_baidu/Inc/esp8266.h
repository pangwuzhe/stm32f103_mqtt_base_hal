#ifndef __ESP8266_H
#define __ESP8266_H

#ifdef __cplusplus
extern "C" {
#endif

int esp8266_init(void);
int esp8266_send(char *mqtt_cmd, uint16_t len);
int esp8266_recv(char *mqtt_recv, uint16_t *len);


#ifdef __cplusplus
}
#endif

#endif /* __ESP8266_H */

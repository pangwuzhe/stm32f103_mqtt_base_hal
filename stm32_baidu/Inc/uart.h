#ifndef __UART_H
#define __UART_H

#ifdef __cplusplus
extern "C" {
#endif

#define RX_BUFF_LEN 	512

int uart_init(void);
int uart_send(uint8_t *buff, uint16_t len);
int uart_recv(uint8_t *buff, uint16_t *len);

#ifdef __cplusplus
}
#endif

#endif /* __UART_H */

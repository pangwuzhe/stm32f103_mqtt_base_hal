# stm32f103_mqtt_base_hal

#### 介绍


使用模块：
- STM32F103C8控制板 
- ESP8266模块 
- DHT11模块 


涉及知识：
- 基于Cube IDE的STM32开发 
- MQTT协议 
- JSON格式解析 
- 百度智能云 
- DHT11温湿度传感器 


